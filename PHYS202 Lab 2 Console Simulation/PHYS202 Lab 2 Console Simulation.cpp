// PHYS202 Lab 2 Console Simulation.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iomanip>
#include <string>
#include <windows.h>
#include <ctype.h>
#include <algorithm>
//#undef max
using namespace std;

HANDLE hConsole;


//declartion of used variables
double torpedoMass = 0.5, electrostaticConstant = 0.00000000899, torpedoAccelerationX, torpedoAccelerationY,
torpedoPositionX = 10000.00, torpedoPositionY = 500.00, torpedoVelocityX = -1200.00, 
torpedoVelocityY = 0.00, torpedoCharge = 16.0, netForceOnTorpedoX, netForceOnTorpedoY;

//returns distance = square root((torpedoX)^2 + ((shieldY - TorpedoY)^2)
//x and y correlate to torpedo position
void calculateDistance(double& distance, double& shieldPositionY, double& torpedoPositionX, double& torpedoPositionY)
{
    distance =  sqrt(pow(torpedoPositionX, 2.0) + pow((shieldPositionY - torpedoPositionY), 2.0));
}

//returns electric field = kq/(r^2)
//k is height, q is electric charge of shield, r is distance from torpedo
void calculateElectricField(double& electricField, double& electrostaticConstant, double& electricCharge, double& distanceFromTorpedo)
{
    electricField =  ((electrostaticConstant * electricCharge) / (pow(distanceFromTorpedo, 2.0)));
}

//returns force(on Torpedo) = qE
//q is torpedo charge, E is deflector shield electric field
void calculateShieldForceOnTorpedo(double& shieldForceOnTorpedo, double& electricField)
{
    shieldForceOnTorpedo = (torpedoCharge * electricField);
}

//class for individual deflector shield containing its variables
//there will be 10 of these in the DeflectorShieldList class
class DeflectorShield
{
public:
    //values for individual deflector shield
    int shieldNumber;
    double electricCharge, forceOnTorpedo, height, distanceFromTorpedo, electricField;
    DeflectorShield* next; //points to deflector shield after current one in the list

    //default constructor for deflector shield, assigns null values
    DeflectorShield()
    {
        shieldNumber = NULL;
        electricCharge = NULL;
        forceOnTorpedo = NULL;
        height = NULL;
        distanceFromTorpedo = NULL;
        electricField = NULL;
        next = NULL;
    }

 /*   //Copy Constructor for sending copy of deflecter shield object to a function
    DeflectorShield(const DeflectorShield& shield) :shieldNumber(shield.shieldNumber), electricCharge(shield.electricCharge),
        forceOnTorpedo(shield.forceOnTorpedo), distanceFromTorpedo(shield.distanceFromTorpedo), electricField(shield.electricField)
    {
        if (shield.next != NULL)
        {
            next = new DeflectorShield;
            *next = *shield.next;
        }
        else
        {
            next = NULL;
        }
    }*/

    //Overload '=' to set deflector shield equal to another existing one
    DeflectorShield& operator=(const DeflectorShield& shield)
    {
        shieldNumber = shield.shieldNumber;
        electricCharge = shield.electricCharge;
        forceOnTorpedo = shield.forceOnTorpedo;
        height = shield.height;
        distanceFromTorpedo = shield.distanceFromTorpedo;
        electricField = shield.electricField;
        *next = *shield.next;
    }
};

//class for circular linked list of 10 Deflector Shields
class DeflectorShieldList
{
public:
    DeflectorShield* head; //pointer object, always points to first shield
    DeflectorShield* current; //points to current viewed shield in list

    DeflectorShieldList() //Default Constructor, assigns NULL pointers
    {
        head = NULL; 
        current = NULL; 
    }

    void calculateNetForceOnTorpedoX(double& netForceOnTorpedoX, double& torpedoPositionX)
    {
        if (current != head)
        {
            current = head;
        }

        double total = 0.0;

        do
        {
            total += cos((torpedoPositionX / current->distanceFromTorpedo));
            current = current->next;
        }while (current != head);

        netForceOnTorpedoX = total;
    }

    void calculateNetForceOnTorpedoY(double& netForceOnTorpedoY, double& torpedoPositionY)
    {
        if (current != head)
        {
            current = head;
        }

        double total = 0.0;

        do
        {
            total += sin(((torpedoPositionY - current->height) / current->distanceFromTorpedo));
            current = current->next;
        } while (current != head);

        netForceOnTorpedoY = total;
    }

    void calculateTorpedoAccelerationX(double& torpedoAccelerationX, double& netForceOnTorpedoX, double& torpedoMass)
    {
        torpedoAccelerationX = (netForceOnTorpedoX / torpedoMass);
    }

    void calculateTorpedoAccelerationY(double& torpedoAccelerationY, double& netForceOnTorpedoY, double& torpedoMass)
    {
        torpedoAccelerationY = (netForceOnTorpedoY / torpedoMass);
    }

    void calculateTorpedoVelocityX(double& torpedoVelocityX, double& torpedoAccelerationX, double& deltat)
    {
        torpedoVelocityX += (torpedoAccelerationX * deltat);
    }

    void calculateTorpedoVelocityY(double& torpedoVelocityY, double& torpedoAccelerationY, double& deltat)
    {
        torpedoVelocityY += (torpedoAccelerationY * deltat);
    }

    void calculateTorpedoPositionX(double& positionX, double& velocityX, double& deltat)
    {
        positionX += (velocityX * deltat);
    }

    void calculateTorpedoPositionY(double& positionY, double& velocityY, double& deltat)
    {
        positionY += (velocityY * deltat);
    }

    //function to make sure user inputs a float for electric charge, rejects other variable types
    double getChargeFloat(int shieldNum)
    {
        switch (shieldNum)
        {
        case 1:
            return .000;
        case 2:
            return .000;
        case 3:
            return .000;
        case 4:
            return .000;
        case 5:
            return .0008;
        case 6:
            return .0008;
        case 7:
            return .000;
        case 8:
            return .000;
        case 9:
            return .000;
        case 10:
            return .000;


        }

        bool enteredFloat = false;
        double enteredCharge = NULL;
        while (!enteredFloat)
        {
            cin >> enteredCharge;
            if (cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "Invalid number, enter a float (decimal) number.\n";
                cout << "Re-enter Deflecter Shield " << shieldNum << " Charge: ";
            }
            else
            {
                return enteredCharge;
            }
        }
    }

    //get all charges of deflector shields from user 
    void getDeflectorShieldCharges()
        {
             double currentCharge = NULL, currentForce = NULL, currentHeight = 100.0,
               currentDistance = NULL, currentElectricField = NULL; //values to be obtained and entered into shield

            for (int currentShield = 1; currentShield <= 10; currentShield++) //loop that repeats 10 times to fill list of shields
            {
                if (head != NULL) //head exists (currentShield > 1)
                {
                    current = current->next; //increments pointer to next shield
                    cout << "Deflector Shield " << currentShield << " Charge: ";
                    currentCharge = getChargeFloat(currentShield);
                    calculateDistance(currentDistance, currentHeight, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(currentElectricField, electrostaticConstant, currentCharge, currentDistance);
                    calculateShieldForceOnTorpedo(currentForce, currentElectricField);

                    current->shieldNumber = currentShield;
                    current->electricCharge = currentCharge;
                    current->forceOnTorpedo = currentForce;
                    current->height = currentHeight;
                    current->distanceFromTorpedo = currentDistance;
                    current->electricField = currentElectricField;
                    
                    if (currentShield != 10)
                    {
                        current->next = new DeflectorShield;
                    }
                    else
                    {
                        current->next = head; //links list back to first deflector shield after list is filled
                        current = current->next; //moves pointer back to head
                    }
                }
                else //head does not exist yet (currentShield = 1)
                {
                    head = new DeflectorShield;
                    cout << "Enter the electric charges in terms of C for the Deflector shields in order from bottom to top, separated by \"Enter\":\n";
                    cout << "Deflector Shield " << currentShield << " Charge: ";
                    currentCharge = getChargeFloat(currentShield);

                    calculateDistance(currentDistance, currentHeight, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(currentElectricField, electrostaticConstant, currentCharge, currentDistance);
                    calculateShieldForceOnTorpedo(currentForce, currentElectricField);

                    head->shieldNumber = currentShield;
                    head->electricCharge = currentCharge;
                    head->forceOnTorpedo = currentForce;
                    head->height = currentHeight;
                    head->distanceFromTorpedo = currentDistance;
                    head->electricField = currentElectricField;
                    head->next = new DeflectorShield;
                    current = head;
                }
                currentHeight += 100.0; //increments height for next deflector shield
            }
        }

        //prints all the values of each deflector shield
        void printAllDeflectorShields()
        {
           if (current != head)
            {
                current = head;
            }

           cout << "At time = 0s:\n";
           cout << "Shield #   Electric Charge(C) Force on Torpedo(N)    Height(m)  Distance From Torpedo(m)   Electric Field(N/C)\n";

            do
            {
                cout << setprecision(1) << current->shieldNumber << "\t\t";
                cout << scientific << current->electricCharge << "\t\t";
                cout << setprecision(6) << current->forceOnTorpedo << "\t\t";
                cout << setprecision(4) << defaultfloat << current->height << "\t\t";
                cout << setprecision(2) << fixed << current->distanceFromTorpedo << "\t\t";
                cout << setprecision(6) << scientific << current->electricField << "\n\n";
                
                current = current->next;
            }while (current != head);

        }

        //deletes all of the shield objects in the list
        void deleteShieldsInList()
        {
            if (current != head)
            {
                current = head;
            }

            DeflectorShield* nextToDelete;
            while (current != NULL)
            {
                if (current->next != NULL)
                {
                    nextToDelete = current->next;
                    delete current;
                    current = nextToDelete;
                }
                else
                {
                    delete current;
                    head = current = nextToDelete = NULL;
                }
            }
            printAllDeflectorShields();
        } 

        //Runs simulation of torpedo hitting wall, outputting all values
        //within each 0.01 s time frame, passes variables by value so the 
        //global (t=0s) variables remain unchanged
        void runTorpedoSimulation(double torpedoMass, double torpedoAccelerationX, double torpedoAccelerationY,
            double torpedoPositionX, double torpedoPositionY, double torpedoVelocityX, double torpedoVelocityY,
            double torpedoCharge, double netForceOnTorpedoX, double netForceOnTorpedoY)
        {
            double deltat = 0.01, timeElapsed = 0.00;

            cout << "\nNow running torpedo simulation...\n";
            cout << "Tor = Torpedo, Es = Electric Field of shield, Fs = force of shield on torpedo\n";
            cout << "Constants are: delta t = 0.01 s, torpedo mass = .5 kg, torpedo charge = +16 C\n";
            cout << "Units are: time = s, Es = Electric Field(shield) = N/C, Fs = Shield Force on Torpedo = N,\nPos = Torpedo Position = m, Vel = Torpedo Velocity m/s, Acc = Torpedo Acceleration = m/s^2, Fnet = Net Force on Torpedo = N\n";
            cout << "\nTime Es(1)   Fs(1)   Es(2)   Fs(2)   Es(3)   Fs(3)   Es(4)   Fs(4)   Es(5)   Fs(5)   Es(6)   Fs(6)   Es(7)   Fs(7)   Es(8)   Fs(8)   Es(9)   Fs(9)   Es(10)  Fs(10)  PosX     PosY    VelX    VelY  AccX  AccY FnetX FnetY\n";
            
            //display all values at t=0s
            cout << setprecision(2) << fixed << timeElapsed << " ";
            cout << setprecision(1) << scientific;
            do
            {
                SetConsoleTextAttribute(hConsole, 11);
                cout << current->electricField << " ";
                SetConsoleTextAttribute(hConsole, 10);
                cout << setprecision(1) << scientific << current->forceOnTorpedo << " ";
                current = current->next;
            } while (current != head);

            calculateNetForceOnTorpedoX(netForceOnTorpedoX, torpedoPositionX);
            calculateNetForceOnTorpedoY(netForceOnTorpedoY, torpedoPositionY);
            calculateTorpedoAccelerationX(torpedoAccelerationX, netForceOnTorpedoX, torpedoMass);
            calculateTorpedoAccelerationY(torpedoAccelerationY, netForceOnTorpedoY, torpedoMass);

            SetConsoleTextAttribute(hConsole, 2);
            cout << setprecision(1) << fixed << torpedoPositionX << "  ";
            SetConsoleTextAttribute(hConsole, 8);
            cout << setprecision(2) << fixed << torpedoPositionY << " ";
            SetConsoleTextAttribute(hConsole, 3);
            cout << torpedoVelocityX << " ";
            SetConsoleTextAttribute(hConsole, 4);
            cout << torpedoVelocityY << " ";
            SetConsoleTextAttribute(hConsole, 5);
            cout << torpedoAccelerationX << " ";
            SetConsoleTextAttribute(hConsole, 6);
            cout << torpedoAccelerationY << " ";
            SetConsoleTextAttribute(hConsole, 7);
            cout << netForceOnTorpedoX << " ";
            SetConsoleTextAttribute(hConsole, 12);
            cout << netForceOnTorpedoY << endl;
            SetConsoleTextAttribute(hConsole, 7);

            timeElapsed += deltat;

            //loop to display all values in 0.01s intervals
            while (timeElapsed < 10.00)
            {
                cout << setprecision(2) << fixed << timeElapsed << " ";
                cout << setprecision(1) << scientific;
                //loop to calculate all deflector shield electric charges and forces on torpedo
                do
                {
                    calculateDistance(current->distanceFromTorpedo, current->height, torpedoPositionX, torpedoPositionY);
                    calculateElectricField(current->electricField, electrostaticConstant, current->electricCharge, current->distanceFromTorpedo);
                    calculateShieldForceOnTorpedo(current->forceOnTorpedo, current->electricField);
                    SetConsoleTextAttribute(hConsole, 11);
                    cout << current->electricField << " ";
                    SetConsoleTextAttribute(hConsole, 10); 
                    cout << current->forceOnTorpedo << " ";
                    current = current->next;
                } while (current != head);

                calculateNetForceOnTorpedoX(netForceOnTorpedoX, torpedoPositionX);
                calculateNetForceOnTorpedoY(netForceOnTorpedoY, torpedoPositionY);
                calculateTorpedoAccelerationX(torpedoAccelerationX, netForceOnTorpedoX, torpedoMass);
                calculateTorpedoAccelerationY(torpedoAccelerationY, netForceOnTorpedoY, torpedoMass);
                calculateTorpedoVelocityX(torpedoVelocityX, torpedoAccelerationX, deltat);
                calculateTorpedoVelocityY(torpedoVelocityY, torpedoAccelerationY, deltat);
                calculateTorpedoPositionX(torpedoPositionX, torpedoVelocityX, deltat);
                calculateTorpedoPositionY(torpedoPositionY, torpedoVelocityY, deltat);

                SetConsoleTextAttribute(hConsole, 2);
                cout << setprecision(2) << fixed << torpedoPositionX << "  ";
                SetConsoleTextAttribute(hConsole, 8);
                cout << torpedoPositionY << " ";
                SetConsoleTextAttribute(hConsole, 3); 
                cout << torpedoVelocityX << " ";
                SetConsoleTextAttribute(hConsole, 4); 
                cout << torpedoVelocityY << " ";
                SetConsoleTextAttribute(hConsole, 5); 
                cout << torpedoAccelerationX << " ";
                SetConsoleTextAttribute(hConsole, 6); 
                cout << torpedoAccelerationY << " ";
                SetConsoleTextAttribute(hConsole, 7);
                cout << netForceOnTorpedoX << " ";
                SetConsoleTextAttribute(hConsole, 12); 
                cout << netForceOnTorpedoY << endl;
                SetConsoleTextAttribute(hConsole, 7);

                timeElapsed += deltat;
            }
            cout.precision(6);
        }

        //reads instructions from the user and executes them
        void processInstructions()
        {
            string instruction;
            bool running = true;
            cout << "Enter an Instruction (Commands are: printshields, newshields, run, help, exit): ";
            cin >> instruction;
            for_each(instruction.begin(), instruction.end(), [](char& c)
            {
                c = ::tolower(static_cast<unsigned char>(c));
            });

            while (running)
            {
                if (instruction == "printshields")
                {
                    printAllDeflectorShields();
                }
                else if (instruction == "newshields")
                {
                    deleteShieldsInList();
                    getDeflectorShieldCharges();
                }
                else if (instruction == "run")
                {
                    runTorpedoSimulation(torpedoMass, torpedoAccelerationX, torpedoAccelerationY,
                        torpedoPositionX, torpedoPositionY, torpedoVelocityX, torpedoVelocityY,
                        torpedoCharge, netForceOnTorpedoX, netForceOnTorpedoY);
                }
                else if (instruction == "exit")
                {
                    running = 0;
                    return;
                }
                else if (instruction == "help")
                {
                    cout << "printshields to print variables of every shield\n";
                    cout << "newshields to delete current shields and replace with new charges\n";
                    cout << "run to run the torpedo simulation\n";
                    cout << "exit to exit the program\n";
                }
                else
                {
                    cout << "Invalid instruction, enter help to see valid instructions";
                    cin.clear();
                    cin.ignore(1000, '\n');
                }

                cout << "\nEnter an instruction: ";
                cin >> instruction;
            }
        }
};

int main()
{
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 7);
    cout << "PHYS 202 Lab 2: Coulong Torpedo Attack\nTimothy Evans & Jake Dannewitz\n\n";

    DeflectorShieldList shieldList;
    shieldList.getDeflectorShieldCharges();
    shieldList.processInstructions();

    //shieldList.deleteShieldsInList();
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
